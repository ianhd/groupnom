﻿var gulp = require('gulp'),
    less = require('gulp-less'),
    path = require('path'),
    minifyCss = require('gulp-minify-css');

gulp.task('less-to-min-css', function () {
    gulp.src('Content/*.less')
      .pipe(less())
      .pipe(minifyCss({ processImport: false }))
      .pipe(gulp.dest('Content'));
});

gulp.task('watch', function () {
    gulp.watch(['Content/*.less'], ['less-to-min-css']);
});