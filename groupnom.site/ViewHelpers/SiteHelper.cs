﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace groupnom.site.ViewHelpers
{
    public static class SiteHelper
    {
        private static string GetResourceSuffix(HttpRequestBase request, string url)
        {
            var delim = "?";
            if (url.Contains("?"))
                delim = "&";
            return request.IsLocal ? delim + "v=localhost" : delim + "v=" + GroupnomApp.Version;
        }

        public static IHtmlString RenderScript(this HtmlHelper htmlHelper, string url)
        {
            var request = htmlHelper.ViewContext.HttpContext.Request;

            url += GetResourceSuffix(request, url);

            return Scripts.Render(url);
        }

        public static IHtmlString RenderStyle(this HtmlHelper htmlHelper, string url)
        {
            var request = htmlHelper.ViewContext.HttpContext.Request;

            url += GetResourceSuffix(request, url);

            return Styles.Render(url);
        }

    }
}