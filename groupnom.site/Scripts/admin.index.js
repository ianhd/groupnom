﻿Vue.use(Toasted); // https://shakee93.github.io/vue-toasted/

var app = new Vue({
    el: '#app',
    mounted: function () {
        this.getMeals();
    },
    methods: {
        showSuccess: function () {
            this.$toasted.show('Done!', { theme: 'primary', className: 'myToast', duration: 1500 })
        },
        getMeals: function () {
            this.$http.get(`${baseUrl}api/meals`)
                .then(function (resp) {
                    this.meals = resp.data;
                }, function (err) {
                    alert("Error: " + err);
                });
        },
        notifyOrganizer: function (mealId) {
            this.$http.post(`${baseUrl}api/notify-organizer`, { mealId: mealId })
                .then(function () {
                    this.showSuccess();
                }, function (err) {
                    alert("Error: " + err);
                });
        }
    },
    data: {
        meals: []
    }
});