﻿$(function () {
    var controls = {
        nav: function() { return $("nav"); },
        toggleMenu: function () { return $(".toggleMenu"); }
    };

    controls.toggleMenu().click(function (e) {
        controls.toggleMenu().toggle();
        controls.nav().toggle();
    });
});