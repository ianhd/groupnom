﻿Vue.use(Toasted); // https://shakee93.github.io/vue-toasted/
//Vue.use(VueClipboard); // https://github.com/Inndy/vue-clipboard2

Vue.component('close-icon', {
    template: '#close-icon'
});

var app = new Vue({
    el: '#app',
    mounted: function () {
        this.getMeal(initData.mealHashId);
    },
    methods: {
        closeAll: function() {
            this.orderModal = false;
            this.paymentModal = false;
            this.restaurantModal = false;
            this.feedbackModal = false;
            this.modal = false;
        },
        getMeal: function (hashId) {
            this.$http.get(`${baseUrl}api/meal/${hashId}`)
                .then(function (resp) {
                    this.meal = resp.data;
                }, function (err) {
                    alert("Error: " + JSON.stringify(err));
                });
        },
        saveOrder: function(order) {
            return this.$http.post(`${baseUrl}api/order`, order);
        },
        httpDeleteOrder: function(orderId) {
            return this.$http.delete(`${baseUrl}api/order/${orderId}`);
        },
        showSuccess: function() {
            this.$toasted.show('Done!', { theme: 'primary', className: 'myToast', duration: 1500 })
        },
        addUpdateOrder: function () {
            this.loadingSaveOrder = true;
            this.order.mealId = this.meal.mealId;

            // get id of order
            this.saveOrder(this.order)
                .then(function (resp) {
                    // editing an order?
                    if (this.editOrderId) {
                        var editOrderId = this.editOrderId; // if you use "this" inside of the .find() below it gets undefined.
                        var order = this.meal.orders.find(function (o) { return o.orderId == editOrderId; });
                        order.name = this.order.name;
                        order.orderDetail = this.order.orderDetail;

                        // reset
                        this.order = {};
                        this.editOrderId = null;
                    } else { // new order
                        this.meal.orders.unshift( // push a copy of the object to sever the pointer
                            { orderId: resp.data, name: this.order.name, orderDetail: this.order.orderDetail }
                        );
                    }

                    this.showSuccess();
                    this.loadingSaveOrder = false;
                    this.orderModal = false;
                    this.paymentModal = true;
                }, function (err) {
                    this.loadingSaveOrder = false;
                    alert(err);
                });
        },
        editOrder: function (orderId) {
            var order = this.meal.orders.find(function (o) { return o.orderId == orderId; });
            this.order = { orderId: order.orderId, name: order.name, orderDetail: order.orderDetail }; // make a copy so we don't risk editing it elsewhere and it's still connected in memory
            this.editOrderId = orderId;
            this.orderModal = true;
        },
        navShowModal: function(modal) {
            this[modal + 'Modal'] = true;
            $(".toggleMenu").eq(0).trigger('click');
        },
        deleteOrder: function () {
            if (!confirm("Are you sure?")) return;
            var index = this.meal.orders.map(x => x.orderId).indexOf(this.editOrderId);
            this.httpDeleteOrder(this.editOrderId)
                .then(function (resp) {
                    if (resp.data) {
                        this.meal.orders.splice(index, 1);
                        this.closeAll();
                        this.showSuccess();
                    }
                });
            //alert(this.editOrderId);
        }
    },
    data: {
        meal: {
            orders: [],
            organizer: {}
        },
        orderModal: false,
        paymentModal: false,
        restaurantModal: false,
        feedbackModal: false,
        modal:false,
        loadingSaveOrder: false,
        moreRestaurantDetails: false,
        editOrderId: 0,
        order: {} // current order being added or edited
    },
    watch: {
        orderModal: function (val) {
            if (val) this.modal = true;
            if (!val) {
                this.editOrderId = 0;
                this.order = {}; // prevent the same order from reappearing if you do edit order -> close, then new order.
            }
            setTimeout(function () {
                $(".orderDetail").focus();
            });
        },
        paymentModal: function (val) {
            if (val) this.modal = true;
        },
        feedbackModal: function (val) {
            if (val) this.modal = true;
        }
        //modal: function (val) {

        //}
    }
});