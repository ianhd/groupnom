﻿Vue.use(Toasted); // https://shakee93.github.io/vue-toasted/

Vue.component('close-icon', {
    template: '#close-icon'
});

var app = new Vue({
    el: '#app',
    mounted: function () {
        this.getMeal(initData.mealHashId);
    },
    methods: {
        closeAll: function () {
            this.orderModal = false;
            this.modal = false;
        },
        tabIs: function (tabIndex) {
            if (tabIndex == this.currentTab) return 'active';
            return '';
        },
        changeTab: function (tabIndex) {
            this.currentTab = tabIndex;
        },
        getMeal: function (hashId) {
            this.$http.get(`${baseUrl}api/meal/${hashId}`)
                .then(function (resp) {
                    this.meal = resp.data;
                }, function (err) {
                    alert("Error: " + JSON.stringify(err));
                });
        },
        goTo: function (url) {
            window.open(url, '_blank');
        },        httpDeleteOrder: function(orderId) {
            return this.$http.delete(`${baseUrl}api/order/${orderId}`);
        },
        httpDeleteOrder: function (orderId) {
            return this.$http.delete(`${baseUrl}api/order/${orderId}`);
        },
        deleteOrder: function () {
            if (!confirm("Are you sure?")) return;
            var index = this.meal.orders.map(x => x.orderId).indexOf(this.editOrderId);
            this.httpDeleteOrder(this.editOrderId)
                .then(function (resp) {
                    if (resp.data) {
                        this.meal.orders.splice(index, 1);
                        this.closeAll();
                        this.showSuccess();
                    }
                });
            //alert(this.editOrderId);
        },
        editOrder: function (orderId) {
            var order = this.meal.orders.find(function (o) { return o.orderId == orderId; });
            this.order = {
                orderId: order.orderId,
                name: order.name,
                orderDetail: order.orderDetail,
                notifyWhenFoodArrives: order.notifyWhenFoodArrives,
                notifyPhoneNumber: order.notifyPhoneNumber
            }; // make a copy so we don't risk editing it elsewhere and it's still connected in memory
            this.editOrderId = orderId;
            this.orderModal = true;
        },
        saveOrder: function (order) {
            return this.$http.post(`${baseUrl}api/order`, order);
        },
        showSuccess: function () {
            this.$toasted.show('Done!', { theme: 'primary', className: 'myToast', duration: 1500 })
        },
        addUpdateOrder: function () {
            this.loadingSaveOrder = true;
            this.order.mealId = this.meal.mealId;

            // get id of order
            this.saveOrder(this.order)
                .then(function (resp) {
                    // editing an order?
                    if (this.editOrderId) {
                        var editOrderId = this.editOrderId; // if you use "this" inside of the .find() below it gets undefined.
                        var order = this.meal.orders.find(function (o) { return o.orderId == editOrderId; });
                        order.name = this.order.name;
                        order.orderDetail = this.order.orderDetail;
                        order.notifyWhenFoodArrives = this.order.notifyWhenFoodArrives;
                        order.notifyPhoneNumber = this.order.notifyPhoneNumber;

                        // reset
                        this.order = {
                            notifyWhenFoodArrives: false
                        };
                        this.editOrderId = null;
                    } else { // new order
                        this.meal.orders.unshift( // push a copy of the object to sever the pointer
                            {
                                orderId: resp.data,
                                name: this.order.name,
                                orderDetail: this.order.orderDetail,
                                notifyWhenFoodArrives: this.order.notifyWhenFoodArrives,
                                notifyPhoneNumber: this.order.notifyPhoneNumber
                            }
                        );
                    }

                    this.showSuccess();
                    this.loadingSaveOrder = false;
                    this.closeAll();
                }, function (err) {
                    this.loadingSaveOrder = false;
                    alert(err);
                });
        }
    },
    data: {
        currentTab: 0,
        meal: {
            orders: [],
            organizer: {}
        },
        orderModal: false,
        modal: false,
        loadingSaveOrder: false,
        editOrderId: 0,
        order: {
            notifyWhenFoodArrives: false
        } // current order being added or edited
    },
    watch: {
        orderModal: function (val) {
            if (val) this.modal = true;
            if (!val) {
                this.editOrderId = 0;
                this.order = {
                    notifyWhenFoodArrives: false
                }; // prevent the same order from reappearing if you do edit order -> close, then new order.
            }
            setTimeout(function () {
                $(".orderDetail").focus();
            });
        }
    }
});