﻿app.factory('mainFactory', function ($http) {
    return {
        //get: function (showId) {
        //    return $http.get(`${baseUrl}api/show-settings/${showId}`)
        //        .then(function (resp) {
        //            return resp.data;
        //        });
        //},
        save: function (item) {
            return $http.post(`${baseUrl}api/meal`, item)
                .then(function (resp) {
                    return resp.data;
                });
        }
        //del: function (item) {
        //    return $http.delete(`${baseUrl}api/show-settings/${item.showSettingId}`);
        //}
    }
});

app.factory('mealAttendeeFactory', function ($http) {
    return {
        save: function (item) {
            return $http.post(`${baseUrl}api/meal-attendee`, item);
        }
    }
});