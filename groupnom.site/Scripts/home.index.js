﻿var googlePlace = null; // there is an angular wrapper for Google Places API Autocomplete, but this way I have full control

var app = angular.module('app', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.directive("autoOpen", ["$parse", function ($parse) {
    return {
        link: function (scope, iElement, iAttrs) {
            var isolatedScope = iElement.isolateScope();
            iElement.on("focus", function () {
                isolatedScope.$apply(function () {
                    $parse("isOpen").assign(isolatedScope, "true");
                });
            });
        }
    };
}]);

app.controller('homeIndexController', ['$scope', 'mainFactory', '$filter', '$timeout', function ($scope, mainFactory, $filter, $timeout) {
    $scope.meal = {};
    $scope.meal.organizer = {};
    $scope.meal.deadlineTime = 10;
    $scope.meal.deadlineAmPm = 'AM';
    //$scope.meal.url = "http://www.blah.com/323";

    // angular-clipboardjs didn't really work on mobile, so let's use original clipboardjs library
    new Clipboard('.btnCopyMealUrl');

    // steps
    $scope.step = 0;
    //$scope.onCopied = function () {
    //    $scope.copySuccess = true;
    //    $timeout(function () {
    //        $scope.copySuccess = false;
    //    }, 1500);
    //};

    // DEBUG
    //$scope.step = 4;
    //$timeout(function () {
    //    $(".btnCopyMealUrl").attr("data-clipboard-text", "post timeout");
    //},1000);

    $scope.numSteps = 5; // includes confirmation div
    $scope.lastStep = $scope.numSteps - 1; // steps are zero based
    $scope.next = function () {
        $scope.step++;
    }
    $scope.previous = function () {
        $scope.step--;
    }

    $scope.today = function () {
        $scope.meal.deadlineDate = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.dateOptions = {
        dateDisabled: false,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    $scope.save = function () {
        $scope.loading = true;

        // figure out unix (sec) of meal deadline
        var date = $filter('date')($scope.meal.deadlineDate, 'yyyyMMdd'); // 20170312
        date = `${date} ${$scope.meal.deadlineTime} ${$scope.meal.deadlineAmPm}`; // 20170312 1 PM
        var dateMoment = moment(date, 'YYYYMMDD ha a');
        $scope.meal.deadlineUnixSec = dateMoment.local().unix();

        // figure out friendly date we can store in the db for the emails to attendees
        var friendlyDate = $filter('date')($scope.meal.deadlineDate, 'M/d/yyyy'); // 3/12/2017
        var timezoneCode = dateMoment.tz(moment.tz.guess()).format('z');
        $scope.meal.deadlineFriendly = `${$scope.meal.deadlineTime} ${$scope.meal.deadlineAmPm} on ${friendlyDate} ${timezoneCode}`; // 1 PM on 3/12/2017
        
        // set restaurant details from Google Places
        var meal = $scope.meal;
        meal.restaurantName = googlePlace.name;
        meal.restaurantPhoneNumber = googlePlace.formatted_phone_number;
        meal.restaurantUrl = googlePlace.website;
        meal.googleMapsUrl = googlePlace.url;

        mainFactory.save(meal)
            .then(function (mealUrl) {
                $scope.next(); // go to last step
                $scope.meal.url = mealUrl;
                $(".btnCopyMealUrl").attr("data-clipboard-text", mealUrl);
                $scope.loading = false;
            }, function (err) {
                alert(`Error (womp womp): ${JSON.stringify(err)}.`);
                $scope.loading = false;
            });
    };
}]);