﻿using groupnom.data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace groupnom.site.Controllers
{
    public class HomeController : Controller
    {
        [Route("sign-in")]
        public ActionResult SignIn() {
            return View();
        }

        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("test")]
        public ActionResult Test()
        {
            return View();
        }

        [Route("privacy-policy.html")]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}