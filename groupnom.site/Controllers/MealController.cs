﻿using groupnom.data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace groupnom.site.Controllers
{
    public class MealController : Controller
    {
        [Route("m-{hashId}")]
        public ActionResult Edit2(string hashId)
        {
            var vm = hashId;
            return View("Edit", model: vm);
        }

        [Route("n-{notifyHashId}")]
        public ActionResult NotifyEveryone(string notifyHashId)
        {
            var numberOfNotificationsSent = MealService.NotifyEveryoneThatFoodIsHere(notifyHashId);
            return View();
        }
    }
}