﻿using groupnom.data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace groupnom.site.Controllers
{
    [RoutePrefix("task")]
    public class TaskController : Controller
    {
        [Route("notify-organizers")]
        public ActionResult NotifyOrganizers()
        {
            var organizersNotified = OrganizerService.NotifyOrganizers();
            return Content($"{organizersNotified} organizer(s) notified");
        }

        [Route("notify-attendees")]
        public ActionResult NotifyAttendees()
        {
            var attendeesNotified = AttendeeService.NotifyAttendees();
            return Content($"{attendeesNotified} attendee(s) notified");
        }
    }
}