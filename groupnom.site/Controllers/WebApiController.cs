﻿using groupnom.data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace groupnom.site.Controllers
{
    [RoutePrefix("api")]
    public class WebApiController : ApiController
    {
        [Route("meal"), HttpPost]
        public string Meal(data.DataModels.Meal meal)
        {
            var mealUrl = MealService.Create(
                meal.Organizer,
                meal);

            return mealUrl;
        }

        [Route("meals"), HttpGet]
        public List<data.DataModels.Meal> Meals()
        {
            return MealService.Get();
        }

        [Route("meal/{hashId}"), HttpGet]
        public data.DataModels.Meal Meal(string hashId)
        {
            //System.Threading.Thread.Sleep(3000);
            return MealService.Get(hashId);
        }

        [Route("order/{orderId}"), HttpDelete]
        public bool Order(int orderId)
        {
            return MealService.DeleteOrder(orderId);
        }

        [Route("order"), HttpPost]
        public int Order(data.DataModels.Order req)
        {
            return MealService.SaveOrder(req);
        }

        [Route("sign-in"), HttpPost]
        public void SignIn(data.DataModels.User req)
        {
            UserService.SendSignInLink(req);
        }

        [Route("notify-organizer"), HttpPost]
        public void NotifyOrganizer(data.DataModels.Meal req)
        {
            OrganizerService.NotifyOrganizers(req.MealId);
        }
    }
}
