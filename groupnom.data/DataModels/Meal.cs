﻿using Dapper;
using groupnom.core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace groupnom.data.DataModels
{
    public sealed partial class Meal : BaseModel
    {
        public static int CreateMealAttendee(int mealId, int attendeeId)
        {
            using (var conn = Db.CreateConnection())
            {
                return conn.Query<int>(@"
                    insert into MealAttendee
                        (MealId,AttendeeId)
                    values
                        (@mealId,@attendeeId);SELECT CAST(SCOPE_IDENTITY() as int)
                ", new { mealId, attendeeId }).FirstOrDefault();
            }
        }

        public static Meal GetForMealAttendee(int mealId)
        {
            using (var conn = Db.CreateConnection())
            {
                var meal = conn.Query<Meal>(@"
                    select
                        OrganizerUserId, RestaurantName,RestaurantUrl,RestaurantPhoneNumber,GoogleMapsUrl,DeadlineFriendly
                    from Meal where MealId = @mealId
                ", new { mealId }).FirstOrDefault();

                // I could do this using a splitOn dapper call but I'm being lazy
                meal.Organizer = conn.Query<User>(@"
                    select [Name] from [User] 
                    where UserId=@OrganizerUserId", new { meal.OrganizerUserId }).FirstOrDefault();

                return meal;
            }
        }

        public static bool MarkAsNotified(int mealId)
        {
            using (var conn = Db.CreateConnection())
            {
                return conn.Execute(@"
                    update Meal set OrganizerNotified=1 where MealId = @mealId
                ", new { mealId }) == 1;
            }
        }

        public static List<Order> GetForNotifyingThatFoodIsHere(int mealId)
        {
            using (var conn = Db.CreateConnection())
            {
                var sql = @"
                    select 
                        m.MealId, 
                        m.RestaurantName,
                        o.OrderId,
                        o.[Name], 
                        o.NotifyPhoneNumber
                    from Meal m
                        inner join [Order] o on o.MealId = m.MealId
                    where m.MealId = @mealId
                        and o.NotifyWhenFoodArrives = 1
                        and o.NotifiedWhenFoodArrived = 0
                    ";
                return conn.Query<Order>(sql, new { mealId }).ToList();
            }
        }

        public static List<Meal> Get()
        {
            using (var conn = Db.CreateConnection())
            {
                var sql = @"
                    select 
                        m.MealId, 
                        m.DeadlineFriendly, 
                        m.RestaurantName, 
                        m.DeadlineUnixSec,
                        u.[Name] as OrganizerName
                    from Meal m
                        inner join [User] u on u.UserId = m.OrganizerUserId
                    order by m.DateCreated desc";
                return conn.Query<Meal>(sql).ToList();
            }
        }

        public static bool DeleteOrder(int orderId)
        {
            using (var conn = Db.CreateConnection())
            {
                return conn.Execute("delete from [Order] where OrderId=@orderId", new { orderId }) == 1;
            }
        }

        public static bool MarkOrderAsNotified(int orderId)
        {
            using (var conn = Db.CreateConnection())
            {
                return conn.Execute("update [Order] set NotifiedWhenFoodArrived=1 where OrderId=@orderId", new { orderId }) == 1;
            }
        }

        public static int SaveOrder(Order order)
        {
            using (var conn = Db.CreateConnection())
            {
                if (order.OrderId == default(int))
                {
                    // insert
                    return conn.Query<int>(@"
                        insert into [Order]
                            (MealId,[Name],OrderDetail,NotifyWhenFoodArrives,NotifyPhoneNumber)
                        values 
                            (@MealId,@Name,@OrderDetail,@NotifyWhenFoodArrives,@NotifyPhoneNumber);
                        SELECT CAST(SCOPE_IDENTITY() as int)
                    ", order).FirstOrDefault();
                }
                conn.Execute(@"
                    update [Order] 
                    set 
                        [Name]=@Name,
                        OrderDetail=@OrderDetail,
                        DateModified=getdate(),
                        NotifyWhenFoodArrives=@NotifyWhenFoodArrives,
                        NotifyPhoneNumber=@NotifyPhoneNumber 
                    where OrderId=@OrderId
                ", order);

                return order.OrderId;
            }
        }

        public static Meal Get(int mealId)
        {
            var nowUnixSec = DateTimeOffset.Now.ToUnixTimeSeconds();

            using (var conn = Db.CreateConnection())
            {
                var rtnMeal = new Meal();
                conn.Query<Meal, User, Order, Meal>(@"
                    select 
                        -- Meal
                        m.MealId, m.RestaurantName, m.RestaurantPhoneNumber, m.GoogleMapsUrl, m.OrganizerNote, m.RestaurantUrl,
                        -- User (Organizer)
                        m.OrganizerUserId as UserId, u.[Name], u.EmailAddress, u.VenmoUsername,
                        -- Orders
                        o.OrderId, o.[Name], o.OrderDetail, o.NotifyPhoneNumber, o.NotifyWhenFoodArrives
                    from [User] u
                        inner join Meal m on m.OrganizerUserId = u.UserId
                        left join [Order] o on o.MealId = m.MealId
                    where m.MealId = @mealId
                    order by o.DateCreated desc
                ", (m, u, o) =>
                {
                    if (rtnMeal.MealId == default(int))
                    {
                        rtnMeal = m;
                        rtnMeal.Orders = new List<Order>();
                    }
                    if (rtnMeal.Organizer == null)
                    {
                        rtnMeal.Organizer = u;
                    }
                    if (o!= null && !rtnMeal.Orders.Any(x => x.OrderId == o.OrderId))
                    {
                        rtnMeal.Orders.Add(o);
                    }

                    return m; // first letter
                }, param: new { mealId }, splitOn: "UserId,OrderId");

                return rtnMeal;
            }
        }

        public static List<Meal> GetPendingMeals(int? mealId = null)
        {
            var nowUnixSec = DateTimeOffset.Now.ToUnixTimeSeconds();

            using (var conn = Db.CreateConnection())
            {
                var whereClause = mealId.HasValue ? $"where m.MealId = {mealId}" : "where m.OrganizerNotified = 0 and m.DeadlineUnixSec < @nowUnixSec";
                var sql = @"
                     select 
                        -- Meal
                        m.MealId, m.RestaurantName, m.RestaurantPhoneNumber, m.GoogleMapsUrl,
                        -- User (Organizer)
                        u.UserId, u.[Name], u.EmailAddress,
                        -- Orders
                        o.OrderId, o.[Name], o.OrderDetail
                    from [User] u
                        inner join Meal m on m.OrganizerUserId = u.UserId
                        left join [Order] o on o.MealId = m.MealId
                    {WHERE}
					order by m.MealId, o.OrderDetail
                ";
                sql = sql.Replace("{WHERE}", whereClause);

                var meals = new List<Meal>();
                conn.Query<Meal, User, Order, Meal>(sql, (m, u, o) =>
                {
                    var rtnMeal = meals.FirstOrDefault(x => x.MealId == m.MealId);
                    if (rtnMeal == null)
                    {
                        rtnMeal = m;
                        rtnMeal.Orders = new List<Order>();
                        meals.Add(rtnMeal);
                    }
                    if (rtnMeal.Organizer == null)
                    {
                        rtnMeal.Organizer = u;
                    }
                    if (o != null && !rtnMeal.Orders.Any(x => x.OrderId == o.OrderId))
                    {
                        rtnMeal.Orders.Add(o);
                    }

                    return m; // first letter
                }, param: new { nowUnixSec }, splitOn: "UserId,OrderId");

                return meals;
            }
        }

        public static int Create(Meal source, User organizer)
        {
            using (var conn = Db.CreateConnection())
            {
                var mealId = conn.Query<int>(@"
                    insert into Meal
                        (OrganizerUserId,RestaurantName,RestaurantUrl,RestaurantPhoneNumber,GoogleMapsUrl,OrganizerNote,DeadlineUnixSec,DeadlineFriendly)
                    values
                        (@OrganizerUserId,@RestaurantName,@RestaurantUrl,@RestaurantPhoneNumber,@GoogleMapsUrl,@OrganizerNote,@DeadlineUnixSec,@DeadlineFriendly)
                    ;SELECT CAST(SCOPE_IDENTITY() as int)
                ", new
                {
                    source.OrganizerUserId,
                    source.RestaurantName,
                    source.RestaurantUrl,
                    source.RestaurantPhoneNumber,
                    source.GoogleMapsUrl,
                    source.OrganizerNote,
                    source.DeadlineUnixSec,
                    source.DeadlineFriendly
                }).FirstOrDefault();

                // add first order for this meal, w/ the organizer being the one placing it
                if (source.OrganizerOrder.HasValue() && organizer != null)
                {
                    conn.Execute(@"
                        insert into [Order] (MealId,[Name],OrderDetail)
                        values (@mealId,@Name,@OrganizerOrder)
                    ", new { mealId, organizer.Name, source.OrganizerOrder });
                }

                return mealId;
            }
        }
    }
}
