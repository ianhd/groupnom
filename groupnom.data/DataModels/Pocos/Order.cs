﻿using groupnom.core.Extensions;
using HashidsNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.DataModels
{
    public sealed partial class Order
    {
        public int OrderId { get; set; }
        public int MealId { get; set; }

        public string Name { get; set; }
        public string OrderDetail { get; set; }
        public bool NotifyWhenFoodArrives { get; set; }
        public bool NotifiedWhenFoodArrived { get; set; }
        public string NotifyPhoneNumber { get; set; }

        // to avoid a dapper query on multiple objects:
        public string RestaurantName { get; set; }
    }
}
