﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace groupnom.data.DataModels
{
    public sealed class Connection : IDbConnection
    {
        public DbConnection DbConnection { get; set; }

        public string ConnectionString { get; set; }

        public int ConnectionTimeout { get; private set; }

        public string Database { get; private set; }

        public ConnectionState State { get; private set; }

        public Connection(string connectionString)
        {
            this.ConnectionString = connectionString;
            this.DbConnection = new SqlConnection(connectionString);
        }

        public void Dispose()
        {
            this.DbConnection.Dispose();
        }

        public IDbTransaction BeginTransaction()
        {
            return this.DbConnection.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return this.DbConnection.BeginTransaction(il);
        }

        public void Close()
        {
            this.DbConnection.Close();
        }

        public void ChangeDatabase(string databaseName)
        {
            this.DbConnection.ChangeDatabase(databaseName);
        }

        public IDbCommand CreateCommand()
        {
            return this.DbConnection.CreateCommand();
        }

        public void Open()
        {
            this.DbConnection.Open();
        }
    }
}
