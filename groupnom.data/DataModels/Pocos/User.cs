﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.DataModels
{
    public sealed partial class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string VenmoUsername { get; set; }
        public string VenmoUrl { get
            {
                return $"https://venmo.com/{VenmoUsername}";
            }
        }
    }
}
