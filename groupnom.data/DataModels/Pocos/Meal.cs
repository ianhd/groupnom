﻿using groupnom.core.Extensions;
using HashidsNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.DataModels
{
    public sealed partial class Meal
    {
        public int MealId { get; set; }
        public int OrganizerUserId { get; set; }
        public string RestaurantName { get; set; }
        public string OrganizerNote { get; set; }
        public string RestaurantUrl { get; set; }
        public string RestaurantPhoneNumber { get; set; }
        public string GoogleMapsUrl { get; set; }
        public string OrganizerOrder { get; set; }
        public int DeadlineUnixSec { get; set; }
        public string DeadlineFriendly { get; set; }

        // helpers
        public string RestaurantPhoneNumberForTel
        {
            get
            {
                if (!RestaurantPhoneNumber.HasValue()) return null;
                return RestaurantPhoneNumber.Replace("(", "")
                    .Replace(")", "")
                    .Replace("-", "")
                    .Replace(" ", "");
            }
        }
        public string MealUrl
        {
            get
            {
                if (MealId == default(int)) return null;
                var hashId = Services.HashIdService.Encode(MealId);

                return $"http://groupnom.com/m-{hashId}";
            }
        }
        public string MealNotifyUrl
        {
            get
            {
                if (MealId == default(int)) return null;

                var notifyHashSalt = ConfigurationManager.AppSettings["notifyHashSalt"];
                var notifyHashId = Services.HashIdService.Encode(MealId, notifyHashSalt);

                return $"http://groupnom.com/n-{notifyHashId}";
            }
        }

        // attendees
        public List<Order> Orders { get; set; }

        // meal edit
        public User Organizer { get; set; }

        // internal admin
        public string OrganizerName { get; set; }
    }
}
