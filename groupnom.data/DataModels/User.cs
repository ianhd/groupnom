﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace groupnom.data.DataModels
{
    public sealed partial class User : BaseModel
    {
        public static int Save(User user)
        {
            using (var conn = Db.CreateConnection())
            {
                var id = conn.Query<int>(@"
                    select UserId
                    from [User]
                    where EmailAddress = @EmailAddress
                ", new { user.EmailAddress }).FirstOrDefault();

                // exists, so update while we're at it
                if (id != default(int))
                {
                    conn.Execute(@"
                        update [User] 
                        set Name = @Name, VenmoUsername = @VenmoUsername, DateModified = getdate()
                        where UserId = @UserId
                    ", new { user.VenmoUsername, user.Name, user.UserId });
                    return id;
                }

                // new, so insert
                return conn.Query<int>(@"
                    insert into [User]
                        (Name,EmailAddress,VenmoUsername)
                    values
                        (@Name,@EmailAddress,@VenmoUsername)
                    ;SELECT CAST(SCOPE_IDENTITY() as int)
                ", new { user.Name, user.EmailAddress, user.VenmoUsername }).FirstOrDefault();
            }
        }
    }
}
