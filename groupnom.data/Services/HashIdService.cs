﻿using HashidsNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.Services
{
    public static class HashIdService
    {
        private static string _hashSeed = "g0 h0k135!";

        public static int Decode(string hashId, string hashSaltOverride = null)
        {
            var hashIds = new Hashids(hashSaltOverride ?? _hashSeed);
            return hashIds.Decode(hashId).FirstOrDefault();
        }

        public static string Encode(int id, string hashSaltOverride = null)
        {
            var hashIds = new Hashids(hashSaltOverride ?? _hashSeed);
            return hashIds.Encode(id);
        }
    }
}
