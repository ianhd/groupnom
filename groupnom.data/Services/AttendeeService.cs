﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.Services
{
    public class AttendeeService
    {
        public static int NotifyAttendees()
        {
            // get list of meals
            var meals = DataModels.Meal.GetPendingMeals();

            //  DataModels.Meal.MarkAsNotified(meal.MealId);

            return meals.Count;
        }
    }
}
