﻿using groupnom.core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.data.Services
{
    public class OrganizerService
    {
        public static int NotifyOrganizers(int? mealId = null)
        {
            // get list of meals
            var meals = DataModels.Meal.GetPendingMeals(mealId);

            meals.ForEach(meal =>
            {
                var mailSvc = new core.Net.MailService();

                var subject = $"Orders for {meal.RestaurantName} are in! (email 2 of 2)";

                var breaks = "<br/><br/>";

                var body = new StringBuilder();
                body.Append($"Hi {meal.Organizer.Name},{breaks}");
                body.Append($"The orders are in! For reference, here are the restaurant details:{breaks}");
                body.Append($"{meal.RestaurantName}{breaks}");
                body.Append($"<a href='tel:{meal.RestaurantPhoneNumberForTel}'>{meal.RestaurantPhoneNumber}</a>{breaks}");
                body.Append($"<a href='{meal.GoogleMapsUrl}'>Google Maps Url</a>{breaks}");
                body.Append($"<strong>Orders</strong><br/>");

                body.Append("<ul>");
                meal.Orders.ForEach(order =>
                {
                    if (order.OrderDetail.HasValue())
                    {
                        body.Append($"<li>{order.OrderDetail} ({order.Name})</li>");
                    }
                });
                body.Append("</ul>");
                body.Append("<br/>");

                body.Append($"🔔&nbsp;&nbsp;When the food arrives, you can <a href='{meal.MealNotifyUrl}'>click/tap here to notify everyone</a>.{breaks}");

                body.Append("---<br/>This email is powered by <a href='http://www.groupnom.com'>Groupnom</a>. Organize your team’s lunch like a champ. You’ll be done in minutes - so you can get back to work.");

                mailSvc.Send(
                    meal.Organizer.EmailAddress,
                    meal.Organizer.Name,
                    subject,
                    body.ToString());

                DataModels.Meal.MarkAsNotified(meal.MealId);
            });

            return meals.Count;
        }
    }
}
