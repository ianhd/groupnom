﻿using groupnom.core.Extensions;
using groupnom.data.DataModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Twilio;

namespace groupnom.data.Services
{
    public class MealService
    {
        public static int NotifyEveryoneThatFoodIsHere(string notifyHashId)
        {
            var notifyHashSalt = ConfigurationManager.AppSettings["notifyHashSalt"];
            var mealId = HashIdService.Decode(notifyHashId, notifyHashSalt);

            string accountSid = ConfigurationManager.AppSettings["twilioAccountSid"];
            string authToken = ConfigurationManager.AppSettings["twilioAuthToken"];
            string fromNumber = ConfigurationManager.AppSettings["twilioFromNumber"];

            var twilio = new TwilioRestClient(accountSid, authToken);

            var ordersPendingNotification = Meal.GetForNotifyingThatFoodIsHere(mealId);
            ordersPendingNotification.ForEach(o =>
            {
                var body = $"Hi {o.Name}, your food is here from {o.RestaurantName}!";
                try
                {
                    var toNumber = o.NotifyPhoneNumber.ToCleanPhoneNumber();
                    var message = twilio.SendMessage(fromNumber, $"+1{toNumber}", body);
                    if (message.Status.ToLower() != "failed")
                        Meal.MarkOrderAsNotified(o.OrderId);
                }
                catch {}
            });

            return ordersPendingNotification.Count;
        }

        public static Meal Get(string hashId)
        {
            var mealId = HashIdService.Decode(hashId);
            return Meal.Get(mealId);
        }

        public static List<Meal> Get()
        {
            return Meal.Get();
        }

        public static bool DeleteOrder(int orderId)
        {
            return Meal.DeleteOrder(orderId);
        }

        public static int SaveOrder(Order order)
        {
            return Meal.SaveOrder(order);
        }

        public static string Create(User organizer, Meal meal)
        {
            // make sure venmo user name does not contain @ symbol
            organizer.VenmoUsername = organizer.VenmoUsername.Replace("@", "");

            // save the organizer user and get their id
            var organizerUserId = User.Save(organizer);
            meal.OrganizerUserId = organizerUserId;

            // save meal
            var mealId = Meal.Create(meal, organizer);
            meal.MealId = mealId; // so that meal.MealUrl is available

            // send email to organizer w/ meal url
            var mailSvc = new core.Net.MailService();
            var subject = $"Meal created for {meal.RestaurantName} (email 1 of 2)";
            var endOfLine = "<br/><br/>";
            var body = new StringBuilder();
            body.Append($"Hi {organizer.Name},{endOfLine}");
            body.Append($"Your meal has been created! Check out the link below where you can manage orders and view restaurant details. You'll get email 2 of 2 around {meal.DeadlineFriendly} with a summary of orders, along with a phone number for the restaurant to help you place the order.{endOfLine}");
            body.Append($"<a href='{meal.MealUrl}'>{meal.MealUrl}</a>{endOfLine}");

            body.Append("---<br/>This email is powered by <a href='http://www.groupnom.com'>Groupnom</a>. Organize your team’s lunch like a champ. You’ll be done in minutes - so you can get back to work.");

            mailSvc.Send(
                organizer.EmailAddress,
                organizer.Name,
                subject,
                body.ToString());

            return meal.MealUrl;
        }
    }
}
