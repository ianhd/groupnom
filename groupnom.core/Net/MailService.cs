﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.core.Net
{
    public class MailService
    {
        private SmtpClient _smtpClient;

        public MailService()
        {
            _smtpClient = new SmtpClient("smtp.mandrillapp.com", 587)
            {
                Credentials = new NetworkCredential("Groupnom", "9SbLLytwtHzpQxPLKDs29A"),
                EnableSsl = true
            };
        }

        public void Send(string toAddress, string toDisplayName, string subject, string body)
        {
            var msg = new MailMessage();
            msg.From = new MailAddress("noreply@groupnom.com", "Groupnom");
            msg.To.Add(new MailAddress(toAddress, toDisplayName));
            msg.IsBodyHtml = true;
            msg.Body = body;
            msg.Subject = subject;
            _smtpClient.Send(msg);
        }
    }
}
