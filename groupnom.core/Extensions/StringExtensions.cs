﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace groupnom.core.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string s)
        {
            return !string.IsNullOrEmpty(s) && !string.IsNullOrWhiteSpace(s);
        }
        public static string ToCleanPhoneNumber(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return null;
            }

            return s.Trim().Replace("-", "").Replace(" ", "").Replace(".", "").Replace("(", "").Replace(")", "");
        }
    }
}
